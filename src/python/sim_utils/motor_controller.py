

import numpy as np 


class motor_controller():
    """
    Base class for motor sim.
    Generic interface definitions which both low- and high-fidelity models can use
    """

    __STATE_INDEX_THETA = 0 # Angular Position in state vector
    __STATE_INDEX_OMEGA = 1 # Angular Velocity in state vector


    def __init__(self):
        self.state = np.ndarray([[0],[0]], dtype='float64')
        self.command = 0

    def set_command(self, command, commandType):
        """
        Sets commanded factor (command)
        """
        self.command = command

class motor_factory():
    pass



if __name__ == "__main__":
    pass
