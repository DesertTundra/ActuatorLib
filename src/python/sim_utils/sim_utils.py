
"""
sim_utils executive
"""

import numpy as np


def run_self_test():
    print("sim_utils.py self test")
    pass


if __name__ == "__main__":
    print("sim_utils.py : main")
    run_self_test()