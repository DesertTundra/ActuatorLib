
import numpy as np
import pykalman as pk


# Row definitions - State
POS = 0
VEL = 1
ACC = 2



# Column Definitions - Dimension
Xdim = 0
Ydim = 1
Zdim = 2

class stateVec():

    def __init__(self, initMatrix):

        self.kf = 1
        self.vec = pk.KalmanFilter(initial_state_mean=0, n_dim_obs=2)
        self.m = 1

def run_self_test():
    print("vec_lib self test")

if __name__ == "__main__":
    run_self_test()
