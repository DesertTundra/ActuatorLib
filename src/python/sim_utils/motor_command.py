
"""
Command types to be passable to a motor controller.

TODO finish documentation here
TODO look into refactoring this interface/inheritance to be cleaner 
"""

import math


class commandType():
    """
    Base class defining commanded 
    """
    def __init__(self, 
                commandValue=0.0,
                commandUnits = "Unitless",
                commandLowerLimit = None, # None = no limit
                commandUpperLimit = None):
        self.commandValue = commandValue
        self.commandUnits = commandUnits
        self.commandLowerLimit = commandLowerLimit
        self.commandUpperLimit = commandUpperLimit

class positionCommand(commandType):

    def __init__(self, 
                commandValue = 0.0,
                commandUnits = "Rad",
                commandUpperLimit = math.pi, # cap at half rotation instead of None
                commandLowerLimit = -math.pi):
        super().__init__(self,
                        commandValue=commandValue,
                        commandUnits=commandUnits,
                        commandUpperLimit=commandUpperLimit,
                        commandLowerLimit=commandLowerLimit)
                
class velocityCommand(commandType):

    def __init__(self, 
                commandValue = 0.0,
                commandUnits = "Rad / sec",
                commandUpperLimit = None, # None = no limit
                commandLowerLimit = None):
        super().__init__(self,
                        commandValue=commandValue,
                        commandUnits=commandUnits,
                        commandUpperLimit=commandUpperLimit,
                        commandLowerLimit=commandLowerLimit)   

class torqueCommand(commandType):

    def __init__(self, 
                commandValue = 0.0,
                commandUnits = "Newton-Meters",
                commandUpperLimit = None, # None = no limit
                commandLowerLimit = None):
        super().__init__(self,
                        commandValue=commandValue,
                        commandUnits=commandUnits,
                        commandUpperLimit=commandUpperLimit,
                        commandLowerLimit=commandLowerLimit)   
